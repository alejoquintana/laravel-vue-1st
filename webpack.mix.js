const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles([
    'resource/vendor/css/all.min.css',
    'resource/vendor/css/adminlte.min.css'
    ], 'public/css/plantilla.css')
    .js('resources/js/app.js', 'public/js')
    .scripts([
        'resource/vendor/js/adminlte.min.js',
        'resource/vendor/js/demo.js'
    ], 'public/js/plantilla.js')
    .copy('public/css/bar.css');
